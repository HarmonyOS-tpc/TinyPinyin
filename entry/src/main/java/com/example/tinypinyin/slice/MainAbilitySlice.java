package com.example.tinypinyin.slice;

import com.example.library.github.promeg.pinyinhelper.Pinyin;
import com.example.library.github.promeg.pinyinhelper.PinyinMapDict;
import com.example.tinypinyin.ResourceTable;
import com.example.tinypinyin.utils.LogUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.util.HashMap;
import java.util.Map;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private Button singleWord;
    private Button multiword;
    private Button polyphonicWord;
    private Button charJudge;
    private Button dictionaries;
    private Text result;

    private final static String TAG = "PinYin";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        singleWord = (Button) findComponentById(ResourceTable.Id_single_word);
        multiword = (Button) findComponentById(ResourceTable.Id_multiword);
        polyphonicWord = (Button) findComponentById(ResourceTable.Id_polyphonic_word);
        charJudge = (Button) findComponentById(ResourceTable.Id_char_judge);
        dictionaries = (Button) findComponentById(ResourceTable.Id_dictionaries);
        result = (Text) findComponentById(ResourceTable.Id_result);

        singleWord.setClickedListener(this);
        multiword.setClickedListener(this);
        polyphonicWord.setClickedListener(this);
        charJudge.setClickedListener(this);
        dictionaries.setClickedListener(this);

        // 初始化配置多音字
        Pinyin.init(Pinyin.newConfig().with(new PinyinMapDict() {
            @Override
            public Map<String, String[]> mapping() {
                HashMap<String, String[]> map = new HashMap<String, String[]>();
                map.put("中国重庆", new String[]{"ZHONG", "GUO", "CHONG", "QING"});
                return map;
            }
        }));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_single_word: // 单字
                result.setText(Pinyin.toPinyin("草", ""));
                break;
            case ResourceTable.Id_multiword: // 多字
                result.setText(Pinyin.toPinyin("草原", ""));
                break;
            case ResourceTable.Id_polyphonic_word: // 多音字

                result.setText(Pinyin.toPinyin("中国重庆", ""));

                break;
            case ResourceTable.Id_char_judge: // 字符判断

                String test = "34我23们";
                String bl="";
                char[] chars = test.toCharArray();
                for (char aChar : chars) {
//                    LogUtil.error(TAG, Pinyin.isChinese(aChar) + "");
                    bl+=Pinyin.isChinese(aChar) + "-";
                }

                result.setText(bl);
                break;
            case ResourceTable.Id_dictionaries: // 添加分割符

                result.setText(Pinyin.toPinyin("草原牧羊", "~"));

                break;
        }
    }
}
