/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.tinypinyin.base;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;

/**
 * BaseAbilitySlice
 */
public abstract class BaseAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(getLayout());

        initView();
        initData();
    }

    /**
     *  初始化Layout
     *
     * @return getLayout
     */
    protected abstract int getLayout();

    /**
     * 初始化Veiw
     */
    protected abstract void initView();

    /**
     * Data处理
     */
    protected abstract void initData();

    /**
     * 无传参
     *
     * @param action String
     */
    public void startAbilitySlice(String action) {
        Intent secondIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withAction(action)
                .build();
        secondIntent.setOperation(operation);
        startAbility(secondIntent);
    }

    /**
     * 有传参
     *
     * @param parameters startAbilitySlice
     * @param action  startAbilitySlice
     */
    public void startAbilitySlice(String action, IntentParams parameters) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withAction(action)
                .build();
        intent.setOperation(operation);
        intent.setParams(parameters);
        startAbility(intent);
    }




    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
    }
}
