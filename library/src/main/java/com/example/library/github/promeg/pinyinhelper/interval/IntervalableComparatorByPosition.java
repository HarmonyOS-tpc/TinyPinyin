package com.example.library.github.promeg.pinyinhelper.interval;

import java.util.Comparator;

public class IntervalableComparatorByPosition implements Comparator<Intervalable> {
    public IntervalableComparatorByPosition() {
    }

    public int compare(Intervalable intervalable, Intervalable intervalable2) {
        return intervalable.getStart() - intervalable2.getStart();
    }
}
