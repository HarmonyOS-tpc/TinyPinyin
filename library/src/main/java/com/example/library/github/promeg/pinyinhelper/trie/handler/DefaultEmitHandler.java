package com.example.library.github.promeg.pinyinhelper.trie.handler;



import com.example.library.github.promeg.pinyinhelper.trie.Emit;

import java.util.ArrayList;
import java.util.List;

public class DefaultEmitHandler implements EmitHandler {
    private List<Emit> emits = new ArrayList();

    public DefaultEmitHandler() {
    }

    public void emit(Emit emit) {
        this.emits.add(emit);
    }

    public List<Emit> getEmits() {
        return this.emits;
    }
}
