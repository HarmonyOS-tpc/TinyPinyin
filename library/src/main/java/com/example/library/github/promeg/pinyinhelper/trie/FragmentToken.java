package com.example.library.github.promeg.pinyinhelper.trie;


public class FragmentToken extends Token {
    public FragmentToken(String fragment) {
        super(fragment);
    }

    public boolean isMatch() {
        return false;
    }

    public Emit getEmit() {
        return null;
    }
}
