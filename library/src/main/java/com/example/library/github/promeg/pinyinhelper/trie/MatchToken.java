package com.example.library.github.promeg.pinyinhelper.trie;

public class MatchToken extends Token {
    private Emit emit;

    public MatchToken(String fragment, Emit emit) {
        super(fragment);
        this.emit = emit;
    }

    public boolean isMatch() {
        return true;
    }

    public Emit getEmit() {
        return this.emit;
    }
}
