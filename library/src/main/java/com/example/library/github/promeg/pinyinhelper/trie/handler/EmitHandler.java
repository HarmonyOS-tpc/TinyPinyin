package com.example.library.github.promeg.pinyinhelper.trie.handler;


import com.example.library.github.promeg.pinyinhelper.trie.Emit;

public interface EmitHandler {
    void emit(Emit var1);
}