package com.example.library.github.promeg.pinyinhelper.trie;


import java.util.*;

public class State {
    private final int depth;
    private final State rootState;
    private Map<Character, State> success;
    private State failure;
    private Set<String> emits;

    public State() {
        this(0);
    }

    public State(int depth) {
        this.success = new HashMap();
        this.failure = null;
        this.emits = null;
        this.depth = depth;
        this.rootState = depth == 0 ? this : null;
    }

    private State nextState(Character character, boolean ignoreRootState) {
        State nextState = (State)this.success.get(character);
        if (!ignoreRootState && nextState == null && this.rootState != null) {
            nextState = this.rootState;
        }

        return nextState;
    }

    public State nextState(Character character) {
        return this.nextState(character, false);
    }

    public State nextStateIgnoreRootState(Character character) {
        return this.nextState(character, true);
    }

    public State addState(Character character) {
       State nextState = this.nextStateIgnoreRootState(character);
        if (nextState == null) {
            nextState = new State(this.depth + 1);
            this.success.put(character, nextState);
        }

        return nextState;
    }

    public int getDepth() {
        return this.depth;
    }

    public void addEmit(String keyword) {
        if (this.emits == null) {
            this.emits = new TreeSet();
        }

        this.emits.add(keyword);
    }

    public void addEmit(Collection<String> emits) {
        Iterator var2 = emits.iterator();

        while(var2.hasNext()) {
            String emit = (String)var2.next();
            this.addEmit(emit);
        }

    }

    public Collection<String> emit() {
        return (Collection)(this.emits == null ? Collections.emptyList() : this.emits);
    }

    public State failure() {
        return this.failure;
    }

    public void setFailure(State failState) {
        this.failure = failState;
    }

    public Collection<State> getStates() {
        return this.success.values();
    }

    public Collection<Character> getTransitions() {
        return this.success.keySet();
    }
}
