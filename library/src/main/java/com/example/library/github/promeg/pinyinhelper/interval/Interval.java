package com.example.library.github.promeg.pinyinhelper.interval;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//



public class Interval implements Intervalable {
    private int start;
    private int end;

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public int size() {
        return this.end - this.start + 1;
    }

    public boolean overlapsWith(Interval other) {
        return this.start <= other.getEnd() && this.end >= other.getStart();
    }

    public boolean overlapsWith(int point) {
        return this.start <= point && point <= this.end;
    }

    public boolean equals(Object o) {
        if (!(o instanceof Intervalable)) {
            return false;
        } else {
            Intervalable other = (Intervalable)o;
            return this.start == other.getStart() && this.end == other.getEnd();
        }
    }

    public int hashCode() {
        return this.start % 100 + this.end % 100;
    }

    public int compareTo(Object o) {
        if (!(o instanceof Intervalable)) {
            return -1;
        } else {
            Intervalable other = (Intervalable)o;
            int comparison = this.start - other.getStart();
            return comparison != 0 ? comparison : this.end - other.getEnd();
        }
    }

    public String toString() {
        return this.start + ":" + this.end;
    }
}
