package com.example.library.github.promeg.pinyinhelper.interval;

public interface Intervalable extends Comparable {
    int getStart();

    int getEnd();

    int size();
}
