# TinyPinyin
## 集成配置 

方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码

```
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```


方式二：

```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:TinyPinyin-Library:1.0.4'
```

 

## 使用

 ### 汉字转拼音API

    String tv = Pinyin.toPinyin("草", "");

 ### 判断是否字符
         String test = "34我23们";
         char[] chars = test.toCharArray();
              for (char aChar : chars) {

                  LogUtil.error(TAG, Pinyin.isChinese(aChar) + "");

                  }


 ### 多音字
        Pinyin.init(Pinyin.newConfig().with(new PinyinMapDict() {
                          @Override
                          public Map<String, String[]> mapping() {
                              HashMap<String, String[]> map = new HashMap<String, String[]>();
                              map.put("中国重庆", new String[]{"ZHONG", "GUO", "CHONG", "QING"});
                              return map;
                          }
                      }));

         result.setText(Pinyin.toPinyin("中国重庆", ""));



 ### 添加分隔符

       String tv =   Pinyin.toPinyin("草原牧羊", "~");
